# -*- coding: utf-8 -*-
"""
Created on Wed Mar 31 17:57:54 2021

@author: mussa
"""

from django.urls import path
from . import views



app_name = 'question'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(),
         name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
]
               